Scriptname ZZNCCHorseScript extends ObjectReference  

ZZNCCCarriageSystemScript Property ZZNCCCarriageSystem  Auto 
Actor Property Me Auto

Event OnCellDetach()

	ZZNCCCarriageSystem.SetStage(20)

EndEvent

Event OnUnload()

	ZZNCCCarriageSystem.SetStage(20)

EndEvent

Function StopMoving(bool ShouldStop)

If ShouldStop == true
	Me.setRestrained(true)
endif 

if ShouldStop == false
	me.setRestrained(false)
endif

endFunction