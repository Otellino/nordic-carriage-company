;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 1
Scriptname NCC_TIF__01005F4D Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
ZZNCCCarriageSystem.CartState = 3
ZZNCCCarriageSystem.Destination = 8
ZZNCCCarriageSystem.DestinationPicked = true
ZZNCCCarriageSystem.StartJourney()
Game.GetPlayer().removeItem(gold001, 35)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

ZZNCCCarriageSystemScript Property ZZNCCCarriageSystem  Auto   

MiscObject Property Gold001  Auto  
