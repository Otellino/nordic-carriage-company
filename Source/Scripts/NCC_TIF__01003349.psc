;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 1
Scriptname NCC_TIF__01003349 Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
; test
;ZZNCCCarriageSystem.HorsePreviousVar01 = (myHorse as Actor).getActorValue("variable01") 
;myHorse.setActorValue("variable01", 0)
ZZNCCCarriageSystem.StopHorse(true, myHorse)
myHorse.evaluatePackage()
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Actor Property myHorse  Auto   

Message Property ZZNCCDebugMsg  Auto  

ZZNCCCarriageSystemScript Property ZZNCCCarriageSystem  Auto 

Weapon Property ZZNCCHorseWeight  Auto  

ObjectReference Property myCart  Auto  

Alias Property Horse  Auto  

ReferenceAlias Property Horse1  Auto  
