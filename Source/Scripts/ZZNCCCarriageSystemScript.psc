Scriptname ZZNCCCarriageSystemScript extends Quest  Conditional

import Game
import Utility

Idle Property IdleCartDriverIdle  Auto  
{cart driver idle}

Idle Property OffsetStop  Auto  

Idle Property IdleCartPassengerAIdle  Auto  
Idle Property IdleCartPassengerBIdle  Auto  
Idle Property IdleCartPassengerCIdle  Auto  
Idle Property IdleCartPassengerDIdle  Auto  
Idle Property IdleCartDriverExit  Auto  
Idle Property IdleCartPassengerAExit  Auto  
Idle Property IdleCartPassengerBExit  Auto  
Idle Property IdleCartPassengerCExit  Auto  
Idle Property IdleCartPassengerDExit  Auto  

GlobalVariable Property SittingAngleLimit  Auto  
{global to change the angle you can look while seated}

float fSittingAngleLimitRadians = 2.6180		; angle player is allowed to look while seated in cart (150 degrees)

float Property HorsePreviousVar01 Auto 

Int Property Destination  Auto Conditional  
{1 - Whiterun
2 - Riften
3 - Windhelm
4 - Markarth
5 - Solitude
6 - Dawnstar
7 - Morthal
8 - Falkreath
9 - Winterhold}

bool Property DestinationPicked = false auto

Weapon Property ZZNCCHorseWeight auto

Function StartJourney()

;=== FROM WHITERUN
	if CurrentLocation == 1 ; Whiterun
		myHorse.setActorValue("variable01", 10)
;=== FROM RIFTEN
	elseif CurrentLocation == 2 ; Riften
		myHorse.setActorValue("variable01", 20)
;=== FROM WINDHELM
	elseif CurrentLocation == 3 ; Windhelm
		myHorse.setActorValue("variable01", 30)
;=== FROM MARKARTH
	elseif CurrentLocation == 4 ; Markarth
		myHorse.setActorValue("variable01", 40)
;=== FROM SOLITUDE
	elseif CurrentLocation == 5 ; Solitude
		myHorse.setActorvalue("variable01", 50) ; Start Solitude Exit path
;=== FROM DAWNSTAR
	elseif CurrentLocation == 6 ; Dawnstar
		myHorse.setActorValue("variable01", 60)
;=== FROM MORTHAL
	elseif CurrentLocation == 7 ; Morthal
		myHorse.setActorValue("variable01", 70)
;=== FROM FALKREATH
	elseif CurrentLocation == 8 ; Falkreath
		myHorse.setActorValue("variable01", 80)
;=== FROM WINTERHOLD
	elseif CurrentLocation == 9 ; Winterhold
		myHorse.setActorValue("variable01", 90)
;=== NOT FROM A HOLD
	elseif CurrentLocation == 0 ; Failsafe 
		myHorse.setActorValue("variable01", 9) ; Find the closest crossroad
	endif 

	myHorse.evaluatePackage()
	myHorse.setRestrained(false)
	CartState = 3
	myDriver.say(ZZNCCCarriageSystemCityTopic)

endFunction

Function EndJourney()

	ResetDialogue()
	CurrentLocation = 0
	PlayerInCart = 0
	CartState = 1
	SpeedState = 0
	myHorse.setActorValue("speedMult", 100)
	myHorse.setActorValue("carryWeight", 100)
	DestinationPicked = false
	Destination = 0
	wait(12)
	;myHorse.setActorValue("variable01", 1)
	myHorse.evaluatePackage()

endFunction

Function ArrivedAtDestination(int ArrivalLocation)

	myDriver.say(ZZNCCCarriageSystemBarksArrival)
	Game.DisablePlayerControls()
	wait(2)
	CurrentLocation = ArrivalLocation
	PlayerInCart = 0
	CartState = 1
	SpeedState = 0
	myHorse.setActorValue("speedMult", 100)
	myHorse.setActorValue("carryWeight", 100)
	DestinationPicked = false
	Destination = 0

	Game.GetPlayer().PlayIdle(IdleCartPassengerCExit)
	wait(11)
	Game.GetPlayer().PlayIdle(IdleCartExitInstant)
	wait(0.5)
	CartCamera(false)
	myHorse.setRestrained(true)
	wait(16)
	myHorse.setRestrained(false)
	;wait(10)
	;Game.GetPlayer().PlayIdle(IdleCartExitInstant)
	;CartCamera(false)

endFunction

Function SkipToDestination()

	; no skipping while encumbered (fast travel will fail)
	if Game.GetPlayer().GetActorValue("InventoryWeight") > Game.GetPlayer().getActorValue("CarryWeight")
; 		debug.trace(self + " player is encumbered - do nothing")
		ZZNCCNoFastTravelWhileEncumbered.show()
	else

		Game.DisablePlayerControls()
		FadeToBlackImod.Apply()
		wait(2)
		FadeToBlackImod.PopTo(FadeToBlackHoldImod)
		CurrentLocation = Destination
		PlayerInCart = 0
		CartState = 1
		SpeedState = 0
		myHorse.setActorValue("speedMult", 100)
		myHorse.setActorValue("carryWeight", 100)
		DestinationPicked = false
		Game.GetPlayer().PlayIdle(IdleCartPassengerCExit)
		wait(1)

		if Destination == 1 ; Whiterun
			fastTravel(Whiterun)
		elseif Destination == 2 ; Riften
			fastTravel(Riften)
		elseif Destination == 3 ; Windhelm
			fastTravel(Windhelm)
		elseif Destination == 4 ; Markarth
			fastTravel(Markarth)
		elseif Destination == 5 ; Solitude
			fastTravel(Solitude)
		elseif Destination == 6 ; Dawnstar
			fastTravel(Dawnstar)
		elseif Destination == 7 ; Morthal
			fastTravel(Morthal)
		elseif Destination == 8 ; Falkreath
			fastTravel(Falkreath)
		elseif Destination == 9 ; Winterhold
			fastTravel(Winterhold)
		endif 

		CartCamera(false)

		ZZNCCCarriageSystem.setStage(20)
		Destination = 0
		Game.GetPlayer().PlayIdle(IdleCartExitInstant)
		wait(0.5)
		FadeToBlackHoldImod.PopTo(FadeToBlackBackImod)
		FadeToBlackHoldImod.Remove()
		Game.GetPlayer().PlayIdle(OffsetStop)
	endif

endFunction

Function CartCamera(bool ApplyCamera)

	if ApplyCamera == True
		;Game.ForceFirstPerson()
		Game.SetInChargen(true, true, true)	; disable saving and waiting
		Game.DisablePlayerControls(abMovement = false, abFighting = true, abCamSwitch = false, abLooking = false, abSneaking = true, abMenu = false, abActivate = false, abJournalTabs = false)
		Game.SetHudCartMode(true)
		;Game.ForceFirstPerson()
		; change camera sitting angle limit
		SittingAngleLimit.SetValue(fSittingAngleLimitRadians)
		; cull first person geomerty during cart ride
		ShowFirstPersonGeometry(false)
		Game.SetSittingRotation(-55)
	elseif ApplyCamera == false
		Game.DisablePlayerControls(abLooking = true)
		; reset camera sitting angle limit
		SittingAngleLimit.SetValue(0)
		Game.EnablePlayerControls()
		Game.SetInChargen(false, false, false)	; reenable saving and waiting
		ShowFirstPersonGeometry(true)
		Game.SetHudCartMode(false)
		Game.GetPlayer().PlayIdle(OffsetStop)
		Game.GetPlayer().setVehicle(none)
	endif

EndFunction

Function StopHorse(bool DoStop, actor theHorse) 

	if DoStop == true
		theHorse.setRestrained()
	elseif DoStop == false
		theHorse.setRestrained(false)
	endif

endFunction

Function SpeedUp(bool DoSpeedUp, actor myHorse)

	if DoSpeedUp == true
		myHorse.setActorValue("speedMult", 175)
		myHorse.setActorValue("carryWeight", 101)
		SpeedState = 1
	elseif DoSpeedUp == false
		myHorse.setActorValue("speedMult", 100)
		myHorse.setActorValue("carryWeight", 100)
		SpeedState = 0
	endif

endFunction

Function RideCart(bool DoRide, bool EndRide, actor myHorse, actor myDriver, ObjectReference myCart, bool FastExit = false)

	if DoRide == true ; Enter the cart

		StopHorse(true, myHorse)
		myCart.setMotionType(myCart.Motion_Keyframed)
		Game.GetPlayer().SetVehicle(myCart)
		Game.GetPlayer().PlayIdle(CartTravelPlayerIdle)
		CartCamera(true)
		PlayerInCart = 1
		wait(1)
		StopHorse(false, myHorse)
		myCart.setMotionType(myCart.Motion_Dynamic)
		if DestinationPicked == true && CartState <= 2
			StartJourney()
		endif
		if DestinationPicked == true && CartState == 4
			CartState = 3
		endif
; COMPANION BIT
		actor myFollower =  Follower.GetActorRef()
		if myFollower.GetActorValue("WaitingForPlayer") == 0
			myFollower.EvaluatePackage()
		; try moving near cart to make them not knock over the cart
			;myFollower.MoveTo(myCart, afYOffset = 400.0)
			;utility.wait(0.5)
			myFollower.SetVehicle(myCart)
			myFollower.PlayIdle(IdleCartPassengerAIdle)
		endif
; END COMPANION BIT

	elseif DoRide == false ; Exit the cart

		if FastExit == false
			if  EndRide == false
				myDriver.say(ZZNCCCarriageSystemWaitBark)
			elseif EndRide == True
				myDriver.say(ZZNCCCarriageSystemEndEarlyBark)
			endif
			StopHorse(true, myHorse)
			myCart.setMotionType(myCart.Motion_Keyframed)
			Game.ForceFirstPerson()	
			Game.GetPlayer().PlayIdle(CartTravelPlayerExit)
			PlayerInCart = 0

			if DestinationPicked == True
				CartState = 4
			elseif DestinationPicked == false
			CartState = 1
			Endif
	; COMPANION BIT
			actor myFollower =  Follower.GetActorRef()
			if myFollower.GetActorValue("WaitingForPlayer") == 0
				myFollower.EvaluatePackage()
				;utility.wait(0.5)
				myFollower.PlayIdle(IdleCartPassengerAExit)
			endif
	; END COMPANION BIT
			wait(11)
			Game.GetPlayer().PlayIdle(IdleCartExitInstant)
			wait(0.5)
			CartCamera(false)
			if myFollower.GetActorValue("WaitingForPlayer") == 0
				myFollower.SetVehicle(none)
				myFollower.MoveTo(Game.GetPlayer(), afYOffset = 400.0)
			endif

			if EndRide == true ; End journey here
				wait(6)
				EndJourney()
				StopHorse(false, myHorse)
				myCart.setMotionType(myCart.Motion_Dynamic)	
			endif
		elseif FastExit == True ; ========= NEW FAST EXIT BIT ========
			if  EndRide == false
				myDriver.say(ZZNCCCarriageSystemWaitBark)
			elseif EndRide == True
				myDriver.say(ZZNCCCarriageSystemEndEarlyBark)
			endif
			StopHorse(true, myHorse)
			CartCamera(false)
			myCart.setMotionType(myCart.Motion_Keyframed)
			Game.GetPlayer().PlayIdle(IdleCartExitInstant)
			Game.GetPlayer().MoveTo(myCart, afYOffset = 200)
			PlayerInCart = 0

			if DestinationPicked == True
				CartState = 4
			elseif DestinationPicked == false
			CartState = 1
			Endif
	; COMPANION BIT
			actor myFollower =  Follower.GetActorRef()
			if myFollower.GetActorValue("WaitingForPlayer") == 0
				myFollower.EvaluatePackage()
				;utility.wait(0.5)
				myFollower.PlayIdle(IdleCartExitInstant)
			endif
	; END COMPANION BIT
			Game.GetPlayer().PlayIdle(IdleCartExitInstant)
			if myFollower.GetActorValue("WaitingForPlayer") == 0
				myFollower.SetVehicle(none)
				myFollower.MoveTo(Game.GetPlayer(), afYOffset = 400.0)
			endif

			if EndRide == true ; End journey here
				EndJourney()
				StopHorse(false, myHorse)
				myCart.setMotionType(myCart.Motion_Dynamic)	
			endif
			; ======= END NEW FAST EXIT BIT
		endif
	endif

endFunction

Function ResetDialogue()

SunnyComment = 0 
RainComment = 0 
SnowComment = 0 
MudcrabComment = 0
MaiqComment = 0
HelgenComment = 0 
FortSungardComment = 0
RiverwoodComment = 0 
ShorsStoneComment = 0
RoriksteadComment = 0
KarthwastenComment = 0
DragonsBridgeComment = 0
HonningbrewComment = 0
DetourComment = 0

endFunction

Int Property CartState  Auto Conditional
{0 - Holding cell
1 - Going to hail marker
2 - Destination picked
3 - Going to destination
4 - Waiting on player}

Int Property PlayerInCart  Auto  Conditional
{0 - Not in the cart
1 - In the cart}

Int Property SpeedState  Auto  Conditional
{0 - Normal speed
1 - Fast speed}

Int Property CurrentLocation  Auto  Conditional
{1 - Whiterun
2 - Riften
3 - Windhelm
4 - Markarth
5 - Solitude
6 - Dawnstar
7 - Morthal
8 - Falkreath
9 - Winterhold}

Actor Property myHorse  Auto  
Actor Property myDriver Auto
ReferenceAlias Property Follower  Auto  

ObjectReference Property Whiterun  Auto  
ObjectReference Property Riften  Auto  
ObjectReference Property Windhelm  Auto  
ObjectReference Property Markarth  Auto  
ObjectReference Property Solitude  Auto  
ObjectReference Property Dawnstar  Auto  
ObjectReference Property Morthal  Auto  
ObjectReference Property Falkreath  Auto  
ObjectReference Property Winterhold  Auto 

Quest Property ZZNCCCarriageSystem Auto
Topic Property ZZNCCCarriageSystemBarksArrival Auto
Topic Property ZZNCCCarriageSystemWaitBark  Auto  
Topic Property ZZNCCCarriageSystemEndEarlyBark  Auto 
Topic Property ZZNCCCarriageSystemCityTopic Auto

Idle Property IdleCartExitInstant  Auto  
Idle Property CartTravelPlayerExit Auto
Idle Property CartTravelPlayerIdle Auto

ImageSpaceModifier Property FadeToBlackImod  Auto  

ImageSpaceModifier Property FadeToBlackHoldImod  Auto  

ImageSpaceModifier Property FadeToBlackBackImod  Auto  

Topic Property CityInfo  Auto  

; Once per trip vars

; Generic comments
int Property SunnyComment = 0 Auto Conditional 
int Property RainComment = 0 Auto Conditional 
int Property SnowComment = 0 Auto Conditional
int Property MudcrabComment = 0 Auto Conditional
int Property MaiqComment = 0 Auto Conditional

; Location comments
int Property HelgenComment = 0 Auto Conditional
int Property FortSungardComment = 0 Auto Conditional
int Property RiverwoodComment = 0 Auto Conditional
int Property ShorsStoneComment = 0 Auto Conditional
int Property RoriksteadComment = 0 Auto Conditional
int Property KarthwastenComment = 0 Auto Conditional
int Property DragonsBridgeComment = 0 Auto Conditional
int Property HonningbrewComment = 0 Auto Conditional

; Detour
int Property DetourComment = 0 Auto Conditional

Message Property ZZNCCNoFastTravelWhileEncumbered  Auto  
