Scriptname ZZNCCCarriageDriverScript extends ObjectReference  

Idle Property IdleCartDriverIdle  Auto  
{cart driver idle}
Idle Property IdleCartDriverExit  Auto 

ZZNCCCarriageSystemScript Property ZZNCCCarriageSystem  Auto  

Actor meAsActor

ObjectReference Property myCart  Auto  

event OnInit()
	meAsActor = (self as ObjectReference) as Actor
	
endEvent
;/
Event OnActivate(ObjectReference akActionRef)

	AttachToCart(True)

EndEvent

function AttachToCart(bool bDoAttach)
; 	debug.trace(self + " AttachToCart = " + bDoAttach)
	if bDoAttach
		; attach me to my cart and make me the driver
		meAsActor.SetActorValue("variable01", 1)
		meAsActor.SetVehicle(myCart)
		meAsActor.PlayIdle(IdleCartDriverIdle)
	else
		; unattach me from cart
		meAsActor.SetVehicle(none)
		meAsActor.SetActorValue("variable01", 0)
		meAsActor.PlayIdle(IdleCartDriverExit)
	endif
endFunction

/;

Event OnCellDetach()

	ZZNCCCarriageSystem.SetStage(20)

EndEvent

Event OnLocationChange(Location akOldLoc, Location akNewLoc)
	if akNewLoc && ZZNCCCarriageSystem.CartState == 3 && meAsActor.IsInDialogueWithPlayer() == 0
; 		debug.trace(self + "OnLocationChange akNewLoc=" + akNewLoc)
			meAsActor.say(AmbientCommentTopic)
	endif
endEvent

Topic Property CityCommentTopic  Auto  
Topic Property AmbientCommentTopic  Auto  
