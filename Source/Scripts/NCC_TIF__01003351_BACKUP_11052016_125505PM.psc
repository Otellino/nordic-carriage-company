;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 5
Scriptname NCC_TIF__01003351 Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_4
Function Fragment_4(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
; Brace cart for going
ZZNCCCarriageSystem.CartState = 2
ZZNCCCarriageSystem.Destination = 1
ZZNCCCarriageSystem.DestinationPicked = true
ZZNCCDebugMsg.show()
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

ZZNCCCarriageSystemScript Property ZZNCCCarriageSystem  Auto   

Message Property ZZNCCDebugMsg  Auto  
