Scriptname ZZNCCCartScript extends ObjectReference  

import Utility

;Actor myPlayer = Game.GetPlayer()  

Idle Property IdleCartDriverIdle  Auto  
{cart driver idle}
Idle Property CartTravelPlayerEnter Auto
Idle Property CartTravelPlayerIdle Auto
Idle Property CartTravelPlayerExit Auto

ZZNCCCarriageSystemScript Property ZZNCCCarriageSystem  Auto  

bool Property StopMotionFlag  Auto  
{set to true to put cart into an immobile state}

float CurrentVariable01 

Event OnActivate(ObjectReference akActionRef)

	int iButton = ZZNCCCartOptions.show()

	if iButton == 0 ; Skip to destination

		ZZNCCCarriageSystem.skipToDestination()

	elseif iButton == 1 ; Hurry up

		ZZNCCCarriageSystem.SpeedUp(true, myHorse)

	elseif iButton == 2 ; Slow Down

		ZZNCCCarriageSystem.SpeedUp(false, myHorse)

	elseif iButton == 3 ; Exit cart

		ZZNCCCarriageSystem.RideCart(false, false, myHorse, myDriver, self, true)

	elseif iButton == 4 ; Enter cart

		ZZNCCCarriageSystem.RideCart(true, false, myHorse, myDriver, self)

	elseif iButton == 5 ; End journey here

		ZZNCCCarriageSystem.RideCart(false, true, myHorse, myDriver, self)

	elseif iButton == 6 ; Nothing

		;ZZNCCDebugMsg.Show(myHorse.getActorValue("variable01"))

	endif

EndEvent

Actor Property myHorse  Auto  

Actor Property myDriver Auto

Message Property ZZNCCCartOptions  Auto  

Topic Property ZZNCCCarriageSystemWaitBark  Auto  

Topic Property ZZNCCCarriageSystemBarkEndJourney Auto

Message Property ZZNCCDebugMsg  Auto  

