Scriptname ZZNCCArrivalTriggerSCRIPT extends ObjectReference  

ZZNCCCarriageSystemScript Property ZZNCCCarriageSystem  Auto  
Actor Property myHorse Auto

Int Property MyLocation  Auto  Conditional
{1 - Whiterun
2 - Riften
3 - Windhelm
4 - Markarth
5 - Solitude
6 - Dawnstar
7 - Morthal
8 - Falkreath}

Event OnTriggerEnter(ObjectReference akActionRef)

	if akActionRef != myHorse
		return
	elseif akActionRef == myHorse		
		if ZZNCCCarriageSystem.Destination == MyLocation

		ZZNCCCarriageSystem.ArrivedAtDestination(MyLocation)

		endif
	endif

EndEvent