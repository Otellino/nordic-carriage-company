Scriptname ZZNCCHailSignScript extends ObjectReference  

import utility

;Quest Property ZZNCCCarriageSystem  Auto
ZZNCCCarriageSystemScript Property ZZNCCCarriageSystem  Auto    

ObjectReference Property SpawnMarker  Auto  
ObjectReference Property ApproachMarker  Auto  
ObjectReference Property ApproachToMoveMarker  Auto  

ObjectReference Property Horse1  Auto
ObjectReference Property Cart1  Auto
ObjectReference Property Driver1  Auto

Message Property ZZNCCCartAroundMsg  Auto 

Event OnActivate(ObjectReference akActionRef)

If ZZNCCCarriageSystem.CartState == 0
	
	ZZNCCCarriageSystem.CurrentLocation = SignLocation
	ZZNCCCarriageSystem.CartState = 1
	Cart1.SetMotionType(Motion_Keyframed)
	ApproachToMoveMarker.moveTo(ApproachMarker)

	wait(0.2)

	Horse1.moveTo(SpawnMarker)
	Cart1.MoveTo(SpawnMarker)
	Driver1.MoveTo(SpawnMarker)

	wait(0.5)

	ZZNCCCarriageSystem.setStage(10)

	wait(1)

	Cart1.SetMotionType(Motion_Dynamic)
	ZZNCCCarriageSystem.StopHorse(false, Horse1 as actor)
	(Horse1 as Actor).SetActorValue("variable01", 1)
	(Horse1 as Actor).evaluatePackage()

ElseIf ZZNCCCarriageSystem.CartState > 0
	; Cart is in world, do nothing
	ZZNCCCartAroundMsg.show()
Endif

EndEvent


Int Property SignLocation = 0 Auto  
{0 - None
1 - Whiterun
2 - Riften
3 - Windhelm
4 - Markarth
5 - Solitude
6 - Dawnstar
7 - Morthal
8 - Falkreath}
