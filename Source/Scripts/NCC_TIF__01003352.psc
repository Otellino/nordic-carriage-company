;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 2
Scriptname NCC_TIF__01003352 Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_1
Function Fragment_1(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
; Set cart state to 3
; ZZNCCCarriageSystem.CartState = 3
ZZNCCCarriageSystem.Destination = 1
ZZNCCCarriageSystem.DestinationPicked = true
ZZNCCCarriageSystem.StartJourney()
Game.GetPlayer().removeItem(gold001, 35)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

ZZNCCCarriageSystemScript Property ZZNCCCarriageSystem  Auto   

MiscObject Property Gold001  Auto  
