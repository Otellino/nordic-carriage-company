;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 26
Scriptname NCC_QF_ZZNCCCarriageSystem_01000D62 Extends Quest Hidden

;BEGIN ALIAS PROPERTY DriverHolderMarker
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_DriverHolderMarker Auto
;END ALIAS PROPERTY

;BEGIN ALIAS PROPERTY Horse1
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_Horse1 Auto
;END ALIAS PROPERTY

;BEGIN ALIAS PROPERTY Driver1
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_Driver1 Auto
;END ALIAS PROPERTY

;BEGIN ALIAS PROPERTY NCCAdvert
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_NCCAdvert Auto
;END ALIAS PROPERTY

;BEGIN ALIAS PROPERTY HorseHolderMarker
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_HorseHolderMarker Auto
;END ALIAS PROPERTY

;BEGIN ALIAS PROPERTY Cart1
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_Cart1 Auto
;END ALIAS PROPERTY

;BEGIN ALIAS PROPERTY NoteHolder
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_NoteHolder Auto
;END ALIAS PROPERTY

;BEGIN ALIAS PROPERTY CartHolderMarker
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_CartHolderMarker Auto
;END ALIAS PROPERTY

;BEGIN FRAGMENT Fragment_23
Function Fragment_23()
;BEGIN CODE
if MQ101.isRunning()
; Nothing
else
CourierScript.AddItemToContainer(Alias_NCCAdvert.GetReference())
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_21
Function Fragment_21()
;BEGIN AUTOCAST TYPE ZZNCCCarriageSystemScript
Quest __temp = self as Quest
ZZNCCCarriageSystemScript kmyQuest = __temp as ZZNCCCarriageSystemScript
;END AUTOCAST
;BEGIN CODE
; Stage 20, cleanup and move to holder

; declare all the references we'll be using multiple times
Actor Driver1 = Alias_Driver1.GetActorRef()
Actor Horse1 = Alias_Horse1.GetActorRef()
Actor myPlayer = Game.GetPlayer()
ObjectReference Cart1 = Alias_Cart1.GetRef()

ObjectReference DriverHolder = Alias_DriverHolderMarker.GetRef()
ObjectReference HorseHolder = Alias_HorseHolderMarker.GetRef()
ObjectReference CartHolder = Alias_CartHolderMarker.GetRef()

;Cart1.SetMotionType(Motion_Keyframed)
utility.wait(0.5)

Cart1.MoveTo(CartHolder)
Driver1.MoveTo(HorseHolder)
Horse1.MoveTo(HorseHolder)

kmyQuest.CartState = 0
ZZNCCCartGoneHome.show()
;kmyQuest.EndJourney()

Horse1.setActorValue("variable01", 0)

kmyQuest.CurrentLocation = 0
kmyQuest.PlayerInCart = 0
kmyQuest.SpeedState = 0
kmyQuest.myHorse.setActorValue("speedMult", 100)
kmyQuest.myHorse.setActorValue("carryWeight", 100)
kmyQuest.DestinationPicked = false
kmyQuest.Destination = 0
kmyQuest.myHorse.evaluatePackage()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_19
Function Fragment_19()
;BEGIN AUTOCAST TYPE ZZNCCCarriageSystemScript
Quest __temp = self as Quest
ZZNCCCarriageSystemScript kmyQuest = __temp as ZZNCCCarriageSystemScript
;END AUTOCAST
;BEGIN CODE
; Stage 10, carriage hailed

;disable the HUD, must do this after player controls are disabled or it won't work
; debug.trace(self + "disabling HUD")
;Game.SetHudCartMode()

; declare all the references we'll be using multiple times
Actor Driver1 = Alias_Driver1.GetActorRef()
Actor Horse1 = Alias_Horse1.GetActorRef()
Actor myPlayer = Game.GetPlayer()
ObjectReference Cart1 = Alias_Cart1.GetRef()

; WJS - for Emulation, we need to wait until all actors are loaded before we can put them in the carts through animation
While !(myPlayer.Is3DLoaded()) || !(Driver1.Is3DLoaded())
                Utility.Wait(0.2)
EndWhile


; tether carts & horses
Cart1.TetherToHorse(Alias_Horse1.GetActorRef())
utility.wait(1)
; put everyone on carts

; cart 1
Driver1.SetVehicle(Cart1)

;WJS - Wait a second before playing animations to sit in cart
;WJS - Removing delay for now
;Utility.Wait(1)

; animations - Cart 1
Driver1.PlayIdle(IdleCartDriverSway)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_17
Function Fragment_17()
;BEGIN AUTOCAST TYPE ZZNCCCarriageSystemScript
Quest __temp = self as Quest
ZZNCCCarriageSystemScript kmyQuest = __temp as ZZNCCCarriageSystemScript
;END AUTOCAST
;BEGIN CODE
; Stage 0, dummy for now
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Idle Property IdleCartDriverSway  Auto  

Int Property CartState  Auto Conditional
{0 - In holding cell
1 - Spawned
2 - Waiting for player to get in
3 - Moving}

Message Property ZZNCCCartAroundMsg  Auto  

Message Property ZZNCCCartGoneHome  Auto  

WICourierScript Property CourierScript  Auto  

Quest Property MQ101  Auto  
