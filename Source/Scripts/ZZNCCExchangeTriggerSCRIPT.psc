Scriptname ZZNCCExchangeTriggerSCRIPT extends ObjectReference  

ZZNCCCarriageSystemScript Property ZZNCCCarriageSystem  Auto  
Actor Property myHorse Auto
Message Property ZZNCCDebugMsg Auto
Int Property MyLocation  Auto  Conditional
{}
;/
Destination List
1 - Whiterun
2 - Riften
3 - Windhelm
4 - Markarth
5 - Solitude
6 - Dawnstar
7 - Morthal
8 - Falkreath
9 - Winterhold
/;

Event OnTriggerEnter(ObjectReference akActionRef)

	if akActionRef != myHorse
		return
	elseif akActionRef == myHorse		
		
		if MyLocation == 1 ; Can go to Solitude, 2 or 5
			if ZZNCCCarriageSystem.Destination == 1 ; Going to Whiterun
				myHorse.setActorValue("variable01", 102) ; Travel from 1 to 2
			elseif ZZNCCCarriageSystem.Destination == 2 ; Going to Riften
				myHorse.setActorValue("variable01", 102) ; Travel from 1 to 2
			elseif ZZNCCCarriageSystem.Destination == 3 ; Going to Windhelm
				myHorse.setActorValue("variable01", 105) ; Travel from 1 to 5
			elseif ZZNCCCarriageSystem.Destination == 4 ; Going to Markarth
				myHorse.setActorValue("variable01", 102) ; Travel from 1 to 2
			elseif ZZNCCCarriageSystem.Destination == 5 ; Going to Solitude
				myHorse.setActorValue("variable01", 55) ; Solitude Approach
			elseif ZZNCCCarriageSystem.Destination == 6 ; Going to Dawnstar
				myHorse.setActorValue("variable01", 105) ; Travel from 1 to 5
			elseif ZZNCCCarriageSystem.Destination == 7 ; Going to Morthal
				myHorse.setActorValue("variable01", 105) ; Travel from 1 to 5
			elseif ZZNCCCarriageSystem.Destination == 8 ; Going to Falkreath
				myHorse.setActorValue("variable01", 102) ; Travel from 1 to 2
			elseif ZZNCCCarriageSystem.Destination == 9 ; Winterhold
				myHorse.setActorValue("variable01", 105) ; Travel from 1 to 5
			endif
		endif
		if MyLocation == 2 ; Can go to 1, Markarth North or 3
			if ZZNCCCarriageSystem.Destination == 1 ; Going to Whiterun
				myHorse.setActorValue("variable01", 203) ; Travel from 2 to 3
			elseif ZZNCCCarriageSystem.Destination == 2 ; Going to Riften
				myHorse.setActorValue("variable01", 203) ; Travel from 2 to 3
			elseif ZZNCCCarriageSystem.Destination == 3 ; Going to Windhelm
				myHorse.setActorValue("variable01", 201) ; Travel from 2 to 1
			elseif ZZNCCCarriageSystem.Destination == 4 ; Going to Markarth
				myHorse.setActorValue("variable01", 218) ; Travel from 2 to 18
			elseif ZZNCCCarriageSystem.Destination == 5 ; Going to Solitude
				myHorse.setActorValue("variable01", 201) ; Travel from 2 to 1
			elseif ZZNCCCarriageSystem.Destination == 6 ; Going to Dawnstar
				myHorse.setActorValue("variable01", 201) ; Travel from 2 to 1
			elseif ZZNCCCarriageSystem.Destination == 7 ; Going to Morthal
				myHorse.setActorValue("variable01", 201) ; Travel from 2 to 1
			elseif ZZNCCCarriageSystem.Destination == 8 ; Going to Falkreath
				myHorse.setActorValue("variable01", 203) ; Travel from 2 to 3
			elseif ZZNCCCarriageSystem.Destination == 9 ; Winterhold
				myHorse.setActorValue("variable01", 201) ; Travel from 2 to 1
			endif
		endif
		if MyLocation == 3 ; Can go to 4, 2 or 18
			if ZZNCCCarriageSystem.Destination == 1 ; Going to Whiterun
				myHorse.setActorValue("variable01", 304) ; Travel from 3 to 4
			elseif ZZNCCCarriageSystem.Destination == 2 ; Going to Riften
				myHorse.setActorValue("variable01", 304) ; Travel from 3 to 4
			elseif ZZNCCCarriageSystem.Destination == 3 ; Going to Windhelm
				myHorse.setActorValue("variable01", 304) ; Travel 3 to 4
			elseif ZZNCCCarriageSystem.Destination == 4 ; Going to Markarth
				myHorse.setActorValue("variable01", 318) ; Travel from 3 to 18
			elseif ZZNCCCarriageSystem.Destination == 5 ; Going to Solitude
				myHorse.setActorValue("variable01", 302) ; Travel from 3 to 2
			elseif ZZNCCCarriageSystem.Destination == 6 ; Going to Dawnstar
				myHorse.setActorValue("variable01", 304) ; Travel from 3 to 4
			elseif ZZNCCCarriageSystem.Destination == 7 ; Going to Morthal
				myHorse.setActorValue("variable01", 302) ; Travel from 3 to 2
			elseif ZZNCCCarriageSystem.Destination == 8 ; Going to Falkreath
				myHorse.setActorValue("variable01", 304) ; Travel from 3 to 4
			elseif ZZNCCCarriageSystem.Destination == 9 ; Winterhold
				myHorse.setActorValue("variable01", 304) ; 3 to 4
			endif			
		endif
		if MyLocation == 4 ; Can go to 9, 10 or 14
			if ZZNCCCarriageSystem.Destination == 1 ; Going to Whiterun
				myHorse.setActorValue("variable01", 415) ; Travel from 4 to 15
			elseif ZZNCCCarriageSystem.Destination == 2 ; Going to Riften
				myHorse.setActorValue("variable01", 417) ; Travel from 4 to 17
			elseif ZZNCCCarriageSystem.Destination == 3 ; Going to Windhelm
				myHorse.setActorValue("variable01", 415) ; Travel 4 to 3
			elseif ZZNCCCarriageSystem.Destination == 4 ; Going to Markarth
				myHorse.setActorValue("variable01", 403) ; Travel from 4 to 3
			elseif ZZNCCCarriageSystem.Destination == 5 ; Going to Solitude
				myHorse.setActorValue("variable01", 403) ; Travel from 4 to 3
			elseif ZZNCCCarriageSystem.Destination == 6 ; Going to Dawnstar
				myHorse.setActorValue("variable01", 415) ; Travel from 4 to 15
			elseif ZZNCCCarriageSystem.Destination == 7 ; Going to Morthal
				myHorse.setActorValue("variable01", 403) ; Travel from 4 to 3
			elseif ZZNCCCarriageSystem.Destination == 8 ; Going to Falkreath
				myHorse.setActorValue("variable01", 417) ; Travel from 4 to 17
			elseif ZZNCCCarriageSystem.Destination == 9 ; Winterhold
				myHorse.setActorValue("variable01", 415) ; 4 to 15
			endif			
		endif
		if MyLocation == 5 ; Can go to 1, Morthal or 6
			if ZZNCCCarriageSystem.Destination == 1 ; Going to Whiterun
				myHorse.setActorValue("variable01", 501) ; Travel from 5 to 1
			elseif ZZNCCCarriageSystem.Destination == 2 ; Going to Riften
				myHorse.setActorValue("variable01", 506) ; Travel from 5 to 6
			elseif ZZNCCCarriageSystem.Destination == 3 ; Going to Windhelm
				myHorse.setActorValue("variable01", 506) ; Travel from 5 to 6
			elseif ZZNCCCarriageSystem.Destination == 4 ; Going to Markarth
				myHorse.setActorValue("variable01", 501) ; Travel from 5 to 1
			elseif ZZNCCCarriageSystem.Destination == 5 ; Going to Solitude
				myHorse.setActorValue("variable01", 501) ; Travel from 5 to 1
			elseif ZZNCCCarriageSystem.Destination == 6 ; Going to Dawnstar
				myHorse.setActorValue("variable01", 506) ; Travel from 5 to 6
			elseif ZZNCCCarriageSystem.Destination == 7 ; Going to Morthal
				myHorse.setActorValue("variable01", 75) ; Travel from 5 to Morthal Approach
			elseif ZZNCCCarriageSystem.Destination == 8 ; Going to Falkreath
				myHorse.setActorValue("variable01", 501) ; Travel from 5 to 1
			elseif ZZNCCCarriageSystem.Destination == 9 ; Winterhold
				myHorse.setActorValue("variable01", 506) ; Travel from 5 to 6
			endif
		endif
		if MyLocation == 6 ; Can go to Dawnstar, 5 or 7
			if ZZNCCCarriageSystem.Destination == 1 ; Going to Whiterun
				myHorse.setActorValue("variable01", 607) ; Travel from 6 to 7
			elseif ZZNCCCarriageSystem.Destination == 2 ; Going to Riften
				myHorse.setActorValue("variable01", 607) ; Travel from 6 to 7
			elseif ZZNCCCarriageSystem.Destination == 3 ; Going to Windhelm
				myHorse.setActorValue("variable01", 607) ; Travel from 6 to 7
			elseif ZZNCCCarriageSystem.Destination == 4 ; Going to Markarth
				myHorse.setActorValue("variable01", 605) ; Travel from 6 to 5
			elseif ZZNCCCarriageSystem.Destination == 5 ; Going to Solitude
				myHorse.setActorValue("variable01", 605) ; Travel from 6 to 5
			elseif ZZNCCCarriageSystem.Destination == 6 ; Going to Dawnstar
				myHorse.setActorValue("variable01", 65) ; Travel from 6 to Dawnstar Approach 1
			elseif ZZNCCCarriageSystem.Destination == 7 ; Going to Morthal
				myHorse.setActorValue("variable01", 605) ; Travel from 6 to 5
			elseif ZZNCCCarriageSystem.Destination == 8 ; Going to Falkreath
				myHorse.setActorValue("variable01", 607) ; Travel from 6 to 7
			elseif ZZNCCCarriageSystem.Destination == 9 ; Winterhold
				myHorse.setActorValue("variable01", 607) ; Travel from 6 to 7
			endif
		endif
		if MyLocation == 7 ; Can go to 6, 8 or 9
			if ZZNCCCarriageSystem.Destination == 1 ; Going to Whiterun
				myHorse.setActorValue("variable01", 709) ; Travel from 7 to 9
			elseif ZZNCCCarriageSystem.Destination == 2 ; Going to Riften
				myHorse.setActorValue("variable01", 708) ; Travel from 7 to 8
			elseif ZZNCCCarriageSystem.Destination == 3 ; Going to Windhelm
				myHorse.setActorValue("variable01", 708) ; Travel from 7 to 8
			elseif ZZNCCCarriageSystem.Destination == 4 ; Going to Markarth
				myHorse.setActorValue("variable01", 709) ; Travel from 7 to 9
			elseif ZZNCCCarriageSystem.Destination == 5 ; Going to Solitude
				myHorse.setActorValue("variable01", 706) ; Travel from 7 to 6
			elseif ZZNCCCarriageSystem.Destination == 6 ; Going to Dawnstar
				myHorse.setActorValue("variable01", 706) ; Travel from 7 to 6
			elseif ZZNCCCarriageSystem.Destination == 7 ; Going to Morthal
				myHorse.setActorValue("variable01", 706) ; Travel from 7 to 6
			elseif ZZNCCCarriageSystem.Destination == 8 ; Going to Falkreath
				myHorse.setActorValue("variable01", 709) ; Travel from 7 to 9
			elseif ZZNCCCarriageSystem.Destination == 9 ; Winterhold
				myHorse.setActorValue("variable01", 708) ; Travel from 7 to 8
			endif			
		endif
		if MyLocation == 8 ; Can go to 7, Winterhold or 10
			if ZZNCCCarriageSystem.Destination == 1 ; Going to Whiterun
				myHorse.setActorValue("variable01", 807) ; Travel from 8 to 7
			elseif ZZNCCCarriageSystem.Destination == 2 ; Going to Riften
				myHorse.setActorValue("variable01", 810) ; Travel from 8 to 10
			elseif ZZNCCCarriageSystem.Destination == 3 ; Going to Windhelm
				myHorse.setActorValue("variable01", 810) ; Travel from 8 to 10
			elseif ZZNCCCarriageSystem.Destination == 4 ; Going to Markarth
				myHorse.setActorValue("variable01", 807) ; Travel from 8 to 7
			elseif ZZNCCCarriageSystem.Destination == 5 ; Going to Solitude
				myHorse.setActorValue("variable01", 807) ; Travel from 8 to 7
			elseif ZZNCCCarriageSystem.Destination == 6 ; Going to Dawnstar
				myHorse.setActorValue("variable01", 807) ; Travel from 8 to 7
			elseif ZZNCCCarriageSystem.Destination == 7 ; Going to Morthal
				myHorse.setActorValue("variable01", 807) ; Travel from 8 to 7
			elseif ZZNCCCarriageSystem.Destination == 8 ; Going to Falkreath
				myHorse.setActorValue("variable01", 807) ; Travel from 8 to 7
			elseif ZZNCCCarriageSystem.Destination == 9 ; Winterhold
				myHorse.setActorValue("variable01", 95) ; Travel from 8 to Winterhold Approach
			endif			
		endif
		if MyLocation == 9 ; Can go to 7, Winterhold or 10
			if ZZNCCCarriageSystem.Destination == 1 ; Going to Whiterun
				myHorse.setActorValue("variable01", 915) ; Travel from 9 to 15
			elseif ZZNCCCarriageSystem.Destination == 2 ; Going to Riften
				myHorse.setActorValue("variable01", 912) ; Travel from 9 to 12
			elseif ZZNCCCarriageSystem.Destination == 3 ; Going to Windhelm
				myHorse.setActorValue("variable01", 912) ; Travel from 9 to 12
			elseif ZZNCCCarriageSystem.Destination == 4 ; Going to Markarth
				myHorse.setActorValue("variable01", 915) ; Travel from 9 to 15
			elseif ZZNCCCarriageSystem.Destination == 5 ; Going to Solitude
				myHorse.setActorValue("variable01", 915) ; Travel from 9 to 15
			elseif ZZNCCCarriageSystem.Destination == 6 ; Going to Dawnstar
				myHorse.setActorValue("variable01", 907) ; Travel from 9 to 7
			elseif ZZNCCCarriageSystem.Destination == 7 ; Going to Morthal
				myHorse.setActorValue("variable01", 907) ; Travel from 9 to 7
			elseif ZZNCCCarriageSystem.Destination == 8 ; Going to Falkreath
				myHorse.setActorValue("variable01", 916) ; Travel from 9 to 16
			elseif ZZNCCCarriageSystem.Destination == 9 ; Winterhold
				myHorse.setActorValue("variable01", 907) ; Travel from 9 to 7
			endif			
		endif
		if MyLocation == 10 ; Can go to 8, 11 or 12
			if ZZNCCCarriageSystem.Destination == 1 ; Going to Whiterun
				myHorse.setActorValue("variable01", 1012) ; Travel from 10 to 12
			elseif ZZNCCCarriageSystem.Destination == 2 ; Going to Riften
				myHorse.setActorValue("variable01", 1011) ; Travel from 10 to 11
			elseif ZZNCCCarriageSystem.Destination == 3 ; Going to Windhelm
				myHorse.setActorValue("variable01", 1011) ; Travel from 10 to 11
			elseif ZZNCCCarriageSystem.Destination == 4 ; Going to Markarth
				myHorse.setActorValue("variable01", 1012) ; Travel from 10 to 12
			elseif ZZNCCCarriageSystem.Destination == 5 ; Going to Solitude
				myHorse.setActorValue("variable01", 1008) ; Travel from 10 to 8
			elseif ZZNCCCarriageSystem.Destination == 6 ; Going to Dawnstar
				myHorse.setActorValue("variable01", 1008) ; Travel from 10 to 8
			elseif ZZNCCCarriageSystem.Destination == 7 ; Going to Morthal
				myHorse.setActorValue("variable01", 1008) ; Travel from 10 to 8
			elseif ZZNCCCarriageSystem.Destination == 8 ; Going to Falkreath
				myHorse.setActorValue("variable01", 1012) ; Travel from 10 to 12
			elseif ZZNCCCarriageSystem.Destination == 9 ; Winterhold
				myHorse.setActorValue("variable01", 1008) ; 10 to 8
			endif			
		endif
		if MyLocation == 11 ; Can go to 10, Windhelm or 13
			if ZZNCCCarriageSystem.Destination == 1 ; Going to Whiterun
				myHorse.setActorValue("variable01", 1110) ; Travel from 10 to 12
			elseif ZZNCCCarriageSystem.Destination == 2 ; Going to Riften
				myHorse.setActorValue("variable01", 1113) ; Travel from 11 to 13
			elseif ZZNCCCarriageSystem.Destination == 3 ; Going to Windhelm
				myHorse.setActorValue("variable01", 35) ; Travel to Windhelm Approach
			elseif ZZNCCCarriageSystem.Destination == 4 ; Going to Markarth
				myHorse.setActorValue("variable01", 1110) ; Travel from 11 to 10
			elseif ZZNCCCarriageSystem.Destination == 5 ; Going to Solitude
				myHorse.setActorValue("variable01", 1110) ; Travel from 11 to 10
			elseif ZZNCCCarriageSystem.Destination == 6 ; Going to Dawnstar
				myHorse.setActorValue("variable01", 1110) ; Travel from 11 to 10
			elseif ZZNCCCarriageSystem.Destination == 7 ; Going to Morthal
				myHorse.setActorValue("variable01", 1110) ; Travel from 11 to 10
			elseif ZZNCCCarriageSystem.Destination == 8 ; Going to Falkreath
				myHorse.setActorValue("variable01", 1113) ; Travel from 11 to 13
			elseif ZZNCCCarriageSystem.Destination == 9 ; Winterhold
				myHorse.setActorValue("variable01", 1011) ; 10 to 11
			endif			
		endif
		if MyLocation == 12 ; Can go to 9, 10 or 14
			if ZZNCCCarriageSystem.Destination == 1 ; Going to Whiterun
				myHorse.setActorValue("variable01", 1209) ; Travel from 12 to 09
			elseif ZZNCCCarriageSystem.Destination == 2 ; Going to Riften
				myHorse.setActorValue("variable01", 1214) ; Travel from 12 to 14
			elseif ZZNCCCarriageSystem.Destination == 3 ; Going to Windhelm
				myHorse.setActorValue("variable01", 1210) ; Travel 12 to 10
			elseif ZZNCCCarriageSystem.Destination == 4 ; Going to Markarth
				myHorse.setActorValue("variable01", 1209) ; Travel from 12 to 9
			elseif ZZNCCCarriageSystem.Destination == 5 ; Going to Solitude
				myHorse.setActorValue("variable01", 1209) ; Travel from 12 to 9
			elseif ZZNCCCarriageSystem.Destination == 6 ; Going to Dawnstar
				myHorse.setActorValue("variable01", 1210) ; Travel from 12 to 10
			elseif ZZNCCCarriageSystem.Destination == 7 ; Going to Morthal
				myHorse.setActorValue("variable01", 1209) ; Travel from 12 to 9
			elseif ZZNCCCarriageSystem.Destination == 8 ; Going to Falkreath
				myHorse.setActorValue("variable01", 1209) ; Travel from 12 to 9
			elseif ZZNCCCarriageSystem.Destination == 9 ; Winterhold
				myHorse.setActorValue("variable01", 1210) ; 12 to 10
			endif			
		endif
		if MyLocation == 13 ; Can go to 9, 10 or 14
			if ZZNCCCarriageSystem.Destination == 1 ; Going to Whiterun
				myHorse.setActorValue("variable01", 1314) ; Travel from 13 to 14
			elseif ZZNCCCarriageSystem.Destination == 2 ; Going to Riften
				myHorse.setActorValue("variable01", 25) ; Travel from 13 to Riften North Approach
			elseif ZZNCCCarriageSystem.Destination == 3 ; Going to Windhelm
				myHorse.setActorValue("variable01", 1311) ; Travel 13 to 11
			elseif ZZNCCCarriageSystem.Destination == 4 ; Going to Markarth
				myHorse.setActorValue("variable01", 1314) ; Travel from 13 to 14
			elseif ZZNCCCarriageSystem.Destination == 5 ; Going to Solitude
				myHorse.setActorValue("variable01", 1314) ; Travel from 13 to 14
			elseif ZZNCCCarriageSystem.Destination == 6 ; Going to Dawnstar
				myHorse.setActorValue("variable01", 1311) ; Travel from 13 to 11
			elseif ZZNCCCarriageSystem.Destination == 7 ; Going to Morthal
				myHorse.setActorValue("variable01", 1314) ; Travel from 13 to 14
			elseif ZZNCCCarriageSystem.Destination == 8 ; Going to Falkreath
				myHorse.setActorValue("variable01", 1314) ; Travel from 13 to 14
			elseif ZZNCCCarriageSystem.Destination == 9 ; Winterhold
				myHorse.setActorValue("variable01", 1311) ; 12 to 10
			endif			
		endif
		if MyLocation == 14 ; Can go to 9, 10 or 14
			if ZZNCCCarriageSystem.Destination == 1 ; Going to Whiterun
				myHorse.setActorValue("variable01", 1412) ; Travel from 14 to 12
			elseif ZZNCCCarriageSystem.Destination == 2 ; Going to Riften
				myHorse.setActorValue("variable01", 1413) ; Travel from 14 to 13
			elseif ZZNCCCarriageSystem.Destination == 3 ; Going to Windhelm
				myHorse.setActorValue("variable01", 1413) ; Travel 14 to 13
			elseif ZZNCCCarriageSystem.Destination == 4 ; Going to Markarth
				myHorse.setActorValue("variable01", 1412) ; Travel from 14 to 12
			elseif ZZNCCCarriageSystem.Destination == 5 ; Going to Solitude
				myHorse.setActorValue("variable01", 1412) ; Travel from 14 to 12
			elseif ZZNCCCarriageSystem.Destination == 6 ; Going to Dawnstar
				myHorse.setActorValue("variable01", 1412) ; Travel from 14 to 12
			elseif ZZNCCCarriageSystem.Destination == 7 ; Going to Morthal
				myHorse.setActorValue("variable01", 1412) ; Travel from 14 to 12
			elseif ZZNCCCarriageSystem.Destination == 8 ; Going to Falkreath
				myHorse.setActorValue("variable01", 1412) ; Travel from 14 to 12
			elseif ZZNCCCarriageSystem.Destination == 9 ; Winterhold
				myHorse.setActorValue("variable01", 1413) ; 14 to 13
			endif			
		endif
		if MyLocation == 15 ; Can go to 9, 10 or 14
			if ZZNCCCarriageSystem.Destination == 1 ; Going to Whiterun
				myHorse.setActorValue("variable01", 15) ; Travel from 15 to Whiterun Approach
			elseif ZZNCCCarriageSystem.Destination == 2 ; Going to Riften
				myHorse.setActorValue("variable01", 1509) ; Travel from 15 to 9
			elseif ZZNCCCarriageSystem.Destination == 3 ; Going to Windhelm
				myHorse.setActorValue("variable01", 1509) ; Travel 15 to 09
			elseif ZZNCCCarriageSystem.Destination == 4 ; Going to Markarth
				myHorse.setActorValue("variable01", 1504) ; Travel from 15 to 4
			elseif ZZNCCCarriageSystem.Destination == 5 ; Going to Solitude
				myHorse.setActorValue("variable01", 1504) ; Travel from 15 to 4
			elseif ZZNCCCarriageSystem.Destination == 6 ; Going to Dawnstar
				myHorse.setActorValue("variable01", 1509) ; Travel from 15 to 09
			elseif ZZNCCCarriageSystem.Destination == 7 ; Going to Morthal
				myHorse.setActorValue("variable01", 1509) ; Travel from 15 to 9
			elseif ZZNCCCarriageSystem.Destination == 8 ; Going to Falkreath
				myHorse.setActorValue("variable01", 1509) ; Travel from 15 to 9
			elseif ZZNCCCarriageSystem.Destination == 9 ; Winterhold
				myHorse.setActorValue("variable01", 1509) ; 15 to 9
			endif			
		endif
		if MyLocation == 16 ; Can go to Markarth, 2 or 3
			if ZZNCCCarriageSystem.Destination == 1 ; Going to Whiterun
				myHorse.setActorValue("variable01", 1609) ; Travel from 16 to 9
			elseif ZZNCCCarriageSystem.Destination == 2 ; Going to Riften
				myHorse.setActorValue("variable01", 1609) ; Travel from 16 to 20
			elseif ZZNCCCarriageSystem.Destination == 3 ; Going to Windhelm
				myHorse.setActorValue("variable01", 1609) ; Travel 16 to 9
			elseif ZZNCCCarriageSystem.Destination == 4 ; Going to Markarth
				myHorse.setActorValue("variable01", 1617) ; Travel from 16 to 17
			elseif ZZNCCCarriageSystem.Destination == 5 ; Going to Solitude
				myHorse.setActorValue("variable01", 1617) ; Travel from 16 to 17
			elseif ZZNCCCarriageSystem.Destination == 6 ; Going to Dawnstar
				myHorse.setActorValue("variable01", 1609) ; Travel from 16 to 9
			elseif ZZNCCCarriageSystem.Destination == 7 ; Going to Morthal
				myHorse.setActorValue("variable01", 1617) ; Travel from 16 to 17
			elseif ZZNCCCarriageSystem.Destination == 8 ; Going to Falkreath
				myHorse.setActorValue("variable01", 1617) ; Travel from 16 to 20
			elseif ZZNCCCarriageSystem.Destination == 9 ; Winterhold
				myHorse.setActorValue("variable01", 1609) ; 16 to 9
			endif			
		endif
		if MyLocation == 17 ; Can go to Markarth, 2 or 3
			if ZZNCCCarriageSystem.Destination == 1 ; Going to Whiterun
				myHorse.setActorValue("variable01", 1716) ; Travel from 17 to 16
			elseif ZZNCCCarriageSystem.Destination == 2 ; Going to Riften
				myHorse.setActorValue("variable01", 1716) ; Travel from 17 to 16
			elseif ZZNCCCarriageSystem.Destination == 3 ; Going to Windhelm
				myHorse.setActorValue("variable01", 1716) ; Travel 17 to 16
			elseif ZZNCCCarriageSystem.Destination == 4 ; Going to Markarth
				myHorse.setActorValue("variable01", 1704) ; Travel from 17 to 4
			elseif ZZNCCCarriageSystem.Destination == 5 ; Going to Solitude
				myHorse.setActorValue("variable01", 1704) ; Travel from 17 to 4
			elseif ZZNCCCarriageSystem.Destination == 6 ; Going to Dawnstar
				myHorse.setActorValue("variable01", 1716) ; Travel from 17 to 16
			elseif ZZNCCCarriageSystem.Destination == 7 ; Going to Morthal
				myHorse.setActorValue("variable01", 1704) ; Travel from 17 to 4
			elseif ZZNCCCarriageSystem.Destination == 8 ; Going to Falkreath
				myHorse.setActorValue("variable01", 1719) ; Travel from 17 to 19
			elseif ZZNCCCarriageSystem.Destination == 9 ; Winterhold
				myHorse.setActorValue("variable01", 1716) ; 17 to 16
			endif			
		endif
		if MyLocation == 18 ; Can go to Markarth, 2 or 3
			if ZZNCCCarriageSystem.Destination == 1 ; Going to Whiterun
				myHorse.setActorValue("variable01", 1803) ; Travel from 18 to 3
			elseif ZZNCCCarriageSystem.Destination == 2 ; Going to Riften
				myHorse.setActorValue("variable01", 1803) ; Travel from 18 to 3
			elseif ZZNCCCarriageSystem.Destination == 3 ; Going to Windhelm
				myHorse.setActorValue("variable01", 1803) ; Travel 18 to 3
			elseif ZZNCCCarriageSystem.Destination == 4 ; Going to Markarth
				myHorse.setActorValue("variable01", 45) ; Travel from 18 to Markarth Approach
			elseif ZZNCCCarriageSystem.Destination == 5 ; Going to Solitude
				myHorse.setActorValue("variable01", 1802) ; Travel from 18 to 2
			elseif ZZNCCCarriageSystem.Destination == 6 ; Going to Dawnstar
				myHorse.setActorValue("variable01", 1802) ; Travel from 18 to 2
			elseif ZZNCCCarriageSystem.Destination == 7 ; Going to Morthal
				myHorse.setActorValue("variable01", 1802) ; Travel from 18 to 2
			elseif ZZNCCCarriageSystem.Destination == 8 ; Going to Falkreath
				myHorse.setActorValue("variable01", 1803) ; Travel from 18 to 3
			elseif ZZNCCCarriageSystem.Destination == 9 ; Winterhold
				myHorse.setActorValue("variable01", 1803) ; 18 to 3
			endif			
		endif
		if MyLocation == 19 ; Can go to Markarth, 2 or 3
			if ZZNCCCarriageSystem.Destination == 1 ; Going to Whiterun
				myHorse.setActorValue("variable01", 1917) ; Travel from 19 to 17
			elseif ZZNCCCarriageSystem.Destination == 2 ; Going to Riften
				myHorse.setActorValue("variable01", 1917) ; Travel from 19 to 17
			elseif ZZNCCCarriageSystem.Destination == 3 ; Going to Windhelm
				myHorse.setActorValue("variable01", 1917) ; Travel 19 to 17
			elseif ZZNCCCarriageSystem.Destination == 4 ; Going to Markarth
				myHorse.setActorValue("variable01", 1917) ; Travel from 19 to 17
			elseif ZZNCCCarriageSystem.Destination == 5 ; Going to Solitude
				myHorse.setActorValue("variable01", 1917) ; Travel from 19 to 17
			elseif ZZNCCCarriageSystem.Destination == 6 ; Going to Dawnstar
				myHorse.setActorValue("variable01", 1917) ; Travel from 19 to 17
			elseif ZZNCCCarriageSystem.Destination == 7 ; Going to Morthal
				myHorse.setActorValue("variable01", 1917) ; Travel from 19 to 17
			elseif ZZNCCCarriageSystem.Destination == 8 ; Going to Falkreath
				myHorse.setActorValue("variable01", 85) ; Travel from 19 to Falkreath West
			elseif ZZNCCCarriageSystem.Destination == 9 ; Winterhold
				myHorse.setActorValue("variable01", 1917) ; 19 to 17
			endif			
		endif
		if MyLocation == 22 ; Can go to Markarth, 2 or 3
			if ZZNCCCarriageSystem.Destination == 1 ; Going to Whiterun
				myHorse.setActorValue("variable01", 2219) ; Travel from 22 to 19
			elseif ZZNCCCarriageSystem.Destination == 2 ; Going to Riften
				myHorse.setActorValue("variable01", 2219) ; Travel from 22 to 19
			elseif ZZNCCCarriageSystem.Destination == 3 ; Going to Windhelm
				myHorse.setActorValue("variable01", 2219) ; Travel 22 to 19
			elseif ZZNCCCarriageSystem.Destination == 4 ; Going to Markarth
				myHorse.setActorValue("variable01", 2219) ; Travel from 22 to 19
			elseif ZZNCCCarriageSystem.Destination == 5 ; Going to Solitude
				myHorse.setActorValue("variable01", 2219) ; Travel from 22 to 19
			elseif ZZNCCCarriageSystem.Destination == 6 ; Going to Dawnstar
				myHorse.setActorValue("variable01", 2219) ; Travel from 22 to 19
			elseif ZZNCCCarriageSystem.Destination == 7 ; Going to Morthal
				myHorse.setActorValue("variable01", 2219) ; Travel from 22 to 19
			elseif ZZNCCCarriageSystem.Destination == 8 ; Going to Falkreath
				myHorse.setActorValue("variable01", 86) ; Travel from 22 to Falkreath East
			elseif ZZNCCCarriageSystem.Destination == 9 ; Winterhold
				myHorse.setActorValue("variable01", 2219) ; 19 to 17
			endif			
		endif

		myHorse.evaluatePackage()
	endif

EndEvent

