# Nordic Carriage Company

The Nordic Carriage Company is a free modification for Bethesda Game Studios' Skyrim: Special Edition. 

# Featureset

Each of the Hold capitals now have a Nordic Carriage Company sign by their entrances. Use these signs to hail a carriage! Talk to the driver, pick which city you want to go to for the measly sum of 35 septims and enjoy the ride.

When on a carriage, you can:

- Skip to your destination
- Speed up (slightly)
- Exit the cart temporarily
- End your journey early
- Enjoy some of the random comments the custom voice-acted carriage driver has to say!

You can only pick other major cities as destinations, but that doesn't mean you can't end your ride early!

# Credits

- wasteofspace95 for the great Nordic Carriage Company logo
- Ali Oram for the voice acting and the absolutely amazing trailer


# Open Source
While I am commited to updating this modification in the future, I am currently focusing my available development time on my Fallout 4 project [America Rising - A Tale of the Enclave]. I've made all source files for the Nordic Carriage Company available here for all to use, whether to create and distribute their own updates or overhauls, or to use as the base for their own Carriage mod. All yours! All I ask is that you leave appropriate credits, as well as a link to the Nordic Carriage Company [NexusMods Page].

[America Rising - A Tale of the Enclave]: <https://bitbucket.org/Otellino/america-rising-a-tale-of-the-enclave/>
[NexusMods Page]: <https://www.nexusmods.com/skyrimspecialedition/mods/4364/?>